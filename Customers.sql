SELECT
users.ID AS userID,
users.user_registered AS created_at,
userinfo.first_name  AS first_name,
userinfo.last_name  AS last_name,
userinfo.email,
postmeta6.meta_value AS preferred_currency_code,
postmeta8.meta_value AS payment_method,
postmeta9.meta_value AS payment_method_title,
CONCAT(postmeta10.meta_value, '/', postmeta11.meta_value) AS payment_method_reference_id,
'on' AS auto_collection,
'taxable' AS taxability,
'fi' AS locale,
postmeta2.meta_value AS billing_address_firstname,
postmeta3.meta_value AS billing_address_lastname,
postmeta7.meta_value AS billing_email,
postmeta5.meta_value AS billing_company,
'Finland' AS billing_country
FROM wp_sijfi_posts posts
LEFT JOIN wp_sijfi_postmeta postmeta1 ON posts.id = postmeta1.post_id AND postmeta1.meta_key = '_customer_user'
LEFT JOIN wp_sijfi_postmeta postmeta2 ON posts.id = postmeta2.post_id AND postmeta2.meta_key = '_billing_first_name'
LEFT JOIN wp_sijfi_postmeta postmeta3 ON posts.id = postmeta3.post_id AND postmeta3.meta_key = '_billing_last_name'
LEFT JOIN wp_sijfi_postmeta postmeta5 ON posts.id = postmeta5.post_id AND postmeta5.meta_key = '_billing_company'
LEFT JOIN wp_sijfi_postmeta postmeta6 ON posts.id = postmeta6.post_id AND postmeta6.meta_key = '_order_currency'
LEFT JOIN wp_sijfi_postmeta postmeta7 ON posts.id = postmeta7.post_id AND postmeta7.meta_key = '_billing_email'
LEFT JOIN wp_sijfi_postmeta postmeta8 ON posts.id = postmeta8.post_id AND postmeta8.meta_key = '_payment_method_title'
LEFT JOIN wp_sijfi_postmeta postmeta9 ON posts.id = postmeta9.post_id AND postmeta9.meta_key = '_payment_method'
LEFT JOIN wp_sijfi_postmeta postmeta10 ON posts.id = postmeta10.post_id AND postmeta10.meta_key = '_stripe_customer_id'
LEFT JOIN wp_sijfi_postmeta postmeta11 ON posts.id = postmeta11.post_id AND postmeta11.meta_key = '_stripe_source_id'
LEFT JOIN wp_sijfi_postmeta postmeta12 ON posts.id = postmeta12.post_id AND postmeta12.meta_key = 'is_vat_exempt'
LEFT JOIN wp_sijfi_users users ON CAST(users.id AS CHAR(5)) = postmeta1.meta_value
LEFT JOIN wp_sijfi_groups_user_group usergroup ON users.id = usergroup.user_id
LEFT JOIN wp_sijfi_wc_customer_lookup userinfo ON users.ID = userinfo.user_id 
LEFT JOIN wp_sijfi_usermeta usermeta1 ON users.ID  = usermeta1.user_id AND usermeta1.meta_key = 'locale'
WHERE usergroup.group_id = 2
AND users.ID NOT IN (SELECT usergroup1.user_id FROM wp_sijfi_groups_user_group usergroup1 WHERE usergroup1.group_id IN (10, 11))
AND posts.post_status IN ('wc-active', 'wc-pending-cancel')
AND posts.post_type = 'shop_subscription'