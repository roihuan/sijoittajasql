SELECT
postmeta1.meta_value AS userID,
posts.post_status AS status,
postmeta2.meta_value AS started_at,
postmeta9.meta_value AS cancelled_at,
CASE WHEN postmeta5.meta_value = 'year' 
THEN  DATE_ADD(postmeta4.meta_value, INTERVAL -1 YEAR) 
ELSE  DATE_ADD(postmeta4.meta_value, INTERVAL -1 MONTH) 
END AS current_term_start,
postmeta4.meta_value AS current_term_end,
postmeta5.meta_value AS billing_period,
orderitems.order_item_name AS item_price_id,
1 AS quantity,
postmeta6.meta_value AS unit_price,
orderitems2.order_item_name AS coupon_name,
postmeta7.meta_value AS coupon_discount,
postmeta8.meta_value AS coupon_discount_tax
FROM wp_sijfi_posts posts
LEFT JOIN wp_sijfi_postmeta postmeta1 ON posts.id = postmeta1.post_id AND postmeta1.meta_key = '_customer_user'
LEFT JOIN wp_sijfi_postmeta postmeta2 ON posts.id = postmeta2.post_id AND postmeta2.meta_key = '_schedule_start'
LEFT JOIN wp_sijfi_postmeta postmeta3 ON posts.id = postmeta3.post_id AND postmeta3.meta_key = '_paid_date'
LEFT JOIN wp_sijfi_postmeta postmeta4 ON posts.id = postmeta4.post_id AND postmeta4.meta_key = '_schedule_next_payment'
LEFT JOIN wp_sijfi_postmeta postmeta5 ON posts.id = postmeta5.post_id AND postmeta5.meta_key = '_billing_period'
LEFT JOIN wp_sijfi_postmeta postmeta6 ON posts.id = postmeta6.post_id AND postmeta6.meta_key = '_order_total'
LEFT JOIN wp_sijfi_users users ON CAST(users.id AS CHAR(5)) = postmeta1.meta_value
LEFT JOIN wp_sijfi_groups_user_group usergroup ON users.id = usergroup.user_id
LEFT JOIN wp_sijfi_woocommerce_order_items orderitems ON posts.ID = orderitems.order_id AND orderitems.order_item_type = 'line_item'
LEFT JOIN wp_sijfi_woocommerce_order_items orderitems2 ON posts.ID = orderitems2.order_id AND orderitems2.order_item_type = 'coupon'
LEFT JOIN wp_sijfi_postmeta postmeta7 ON posts.id = postmeta7.post_id AND postmeta7.meta_key = '_cart_discount'
LEFT JOIN wp_sijfi_postmeta postmeta8 ON posts.id = postmeta8.post_id AND postmeta8.meta_key = '_cart_discount_tax'
LEFT JOIN wp_sijfi_postmeta postmeta9 ON posts.id = postmeta9.post_id AND postmeta9.meta_key = '_schedule_end'
WHERE usergroup.group_id = 2
AND users.ID IN (SELECT usergroup1.user_id FROM wp_sijfi_groups_user_group usergroup1 WHERE usergroup1.group_id IN (10, 11))
AND posts.post_status IN ('wc-active', 'wc-pending-cancel')
AND posts.post_type = 'shop_subscription'